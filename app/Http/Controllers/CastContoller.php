<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CastContoller extends Controller
{
    public function create(){
        return view('casts.create');
    }

    public function store(Request $request){
        dd($request->all());
        $request->validate([
            "name" => 'required',
            "age" => 'required'
        ]);

        $query=DB::table('cast')->insert([
            "nama" => $request["name"],
            "umur" => $request["age"],
            "bio" => $request["bio"]
        ]);
        return redirect('/cast')->with('success', 'Berhasil menambahkan Data Pemeran');
    }
    
    public function index(){
        $cast=DB::table('cast')->get();
        return view('casts.index', compact('cast'));
    }

    public function show($cast_id){
        $cast = DB::table('cast')->where('id', $cast_id)->first();
        return view('casts.show', compact('cast'));
    }

    public function edit($cast_id){
        $cast = DB::table('cast')->where('id', $cast_id)->first();
        return view('casts.edit', compact('cast'));
    }

    public function update($cast_id, Request $request){
        $request->validate([
            "name" => 'required',
            "age" => 'required',
        ]);

        $query = DB::table('cast')
            ->where('id', $cast_id)
            ->update([
                'nama' => $request["name"],
                'umur' => $request["age"],
                'bio' => $request["bio"]
            ]);
        return redirect('/cast');
    }

    public function destroy($cast_id){
        $query = DB::table('cast')->where('id', $cast_id)->delete();
        return redirect('/cast');
    }
}
