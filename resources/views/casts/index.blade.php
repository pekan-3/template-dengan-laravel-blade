@extends('layout.master')

@push('cdn-datatable')
  <link rel="stylesheet" href="{{asset('Admin/dist/css/datatables.min.css')}}">
@endpush

@section('judul')
    Pemeran
@endsection

@section('judul-kartu')
    Pemeran
@endsection

@section('isi')
@if (session('success'))
    <div class="alert alert-success">
      {{session('success')}}
    </div>
@endif
<a href="/cast/create" class="btn btn-success mb-2"><i class="fas fa-plus mr-1"></i>Tambah</a>
<table id="example1" class="table table-bordered table-striped">
    <thead class="thead-light">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Bio</th>
        <th scope="col" style="width">Actions</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key=>$value)
            <tr>
                <td>{{$key + 1}}</th>
                <td>{{$value->nama}}</td>
                <td>{{$value->umur}} Tahun</td>
                <td>{{$value->bio}}</td>
                <td style="display: flex;">
                  <a href="/cast/{{$value->id}}" class="btn btn-primary btn-sm my-1 mr-1" style="height: 31px"><i class="far fa-eye mr-1"></i>Tampil</a>
                  <a href="/cast/{{$value->id}}/edit" class="btn btn-info btn-sm my-1 mr-1" style="height: 31px"><i class="fas fa-pencil-alt mr-1"></i>Ubah</a>
                    <form action="/cast/{{$value->id}}" method="post">
                        @csrf
                        @method('DELETE')
                        {{-- <input type="submit" class="btn btn-danger btn-sm my-1 mr-1" style="height: 31px" value="Delete"> --}}
                        <button type="submit" class="btn btn-danger btn-sm my-1 mr-1"><i class="fas fa-trash mr-1"></i>Hapus</button>
                    </form>
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="5" align="center">No data</td>
            </tr>  
        @endforelse              
    </tbody>
</table>
@endsection

@push('script')
<script src="{{asset('Admin/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('Admin/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
@endpush