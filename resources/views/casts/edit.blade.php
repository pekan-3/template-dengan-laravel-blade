@extends('layout.master')

@section('judul')
    Tambah Pemeran
@endsection

@section('judul-kartu')
    Tambah Data
@endsection

@section('isi')
<div class="card card-primary">
    <!-- form start -->
    <form role="form" action="/cast/{{$cast->id}}" method="POST">
      @csrf
      @method('PUT')
      <div class="card-body">
        <div class="form-group">
          <label for="exampleInputName">Nama</label>
          <input type="text" class="form-control" id="nama" name="name" value="{{old('name', $cast->nama)}}" placeholder="Nama">
          @error('name')
            <div class="alert alert-danger">
              {{ $message }}
            </div>
          @enderror
        </div>
        <div class="form-group">
          <label for="exampleInputAge">Umur</label>
          <input type="text" class="form-control" id="umur" name="age" value="{{old('age', $cast->umur)}}" placeholder="Umur">
          @error('age')
            <div class="alert alert-danger">
              {{ $message }}
            </div>
          @enderror
        </div>
        <div class="form-group">
          <label for="exampleInputBio">Bio</label>
          <textarea class="form-control" rows="3" placeholder="Bio" name="bio">{{old('bio', $cast->bio)}}</textarea>
        </div>
      </div>
      <!-- /.card-body -->
    
      <div class="card-footer">
        <button type="submit" class="btn btn-primary pr-3 pl-3">Ubah</button>
        <a class="btn btn-warning pr-3 pl-3" style="color: white" href="/cast">Batal</a>
      </div>
    </form>
</div>
@endsection