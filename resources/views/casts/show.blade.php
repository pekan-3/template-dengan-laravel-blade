@extends('layout.master')

@section('judul')
    Preview Pemeran
@endsection

@section('judul-kartu')
    Preview Data
@endsection

@section('isi')
<h3 class="profile-username text-center">{{$cast->nama}}</h3> 
<p class="text-muted text-center">{{$cast->umur}} Tahun</p>

<hr>

<strong><i class="far fa-file-alt mr-1"></i> Bio</strong>

<p class="text-muted">{{$cast->bio}}</p>

<hr>

<a href="/cast" class="btn btn-secondary "><b>Kembali</b></a>
@endsection