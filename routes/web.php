<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('page.home');
});

Route::get('/table', function(){
    return view('table.tables');
});

Route::get('/data-tables', function(){
    return view('table.datatable');
});

Route::get('/cast/create', 'CastContoller@create');

Route::post('/cast', 'CastContoller@store');

Route::get('/cast', 'CastContoller@index');

Route::get('/cast/{cast_id}', 'CastContoller@show');

Route::get('/cast/{cast_id}/edit', 'CastContoller@edit');

Route::put('/cast/{cast_id}', 'CastContoller@update');

Route::delete('/cast/{cast_id}', 'CastContoller@destroy');